import unittest
from os import path
from time import sleep  # Sleep is needed to ensure that images are downloaded

from click.testing import CliRunner

from importlib.util import spec_from_loader, module_from_spec
from importlib.machinery import SourceFileLoader

spec = spec_from_loader("sauce_finder",
                        SourceFileLoader("sauce_finder",
                                         "sauce_finder/sauce_finder"))
sauce_finder = module_from_spec(spec)
spec.loader.exec_module(sauce_finder)


class TestSauceFinderCLI(unittest.TestCase):
    # Different URL tests

    def test_with_invalid_url(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            data = ["https://google.com"]
            result = runner.invoke(sauce_finder.main, data)
            self.assertEqual(result.exit_code, 2)

    def test_with_valid_url_100(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            data = ["https://i.imgur.com/eUD3lDZ.jpg"]
            result = runner.invoke(sauce_finder.main, data)
            self.assertEqual(result.exit_code, 0)
            output = "95% similarity\nhttps://danbooru.donmai.us/posts" \
                     "/3473904\n"
            self.assertEqual(result.output, output)

    def test_with_valid_url_100_verbose(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            data = ["https://i.imgur.com/eUD3lDZ.jpg", "-v"]
            result = runner.invoke(sauce_finder.main, data)
            self.assertEqual(result.exit_code, 0)
            output = "95% similarity\n[Safe] | 2552x4096\nhttps://danbooru.d" \
                     "onmai.us/posts/3473904\n"
            self.assertEqual(result.output, output)

    def test_with_valid_url_100_export(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            self.assertFalse(path.exists("file.txt"))
            data = ["https://i.imgur.com/eUD3lDZ.jpg", "-e", "file.txt"]
            result = runner.invoke(sauce_finder.main, data)
            self.maxDiff = None
            self.assertEqual(result.exit_code, 0)
            # Check that file exists
            self.assertTrue(path.exists("file.txt"))
            # Check file content
            test_content = '{\n    "link": "https://danbooru.donmai.us/posts' \
                           '/3473904",\n    "similarity": "95",\n    "rating' \
                           '": "[Safe]",\n    "size": {\n        "width": "2' \
                           '552",\n        "height": "4096"\n    }\n}'
            content = ""
            with open("file.txt", "r") as _:
                content = _.read()
            self.assertEqual(content, test_content)

    def test_download_with_100(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Test downloading
            data = ["https://i.imgur.com/eUD3lDZ.jpg", "-d"]
            result = runner.invoke(sauce_finder.main, data)
            sleep(3)
            self.assertEqual(result.exit_code, 0)
            image_name = "__monika_doki_doki_literature_club_drawn_by_tsuki" \
                         "maru__sample-88798653a551aee9af89be16310ec9ec"
            self.assertTrue(path.exists("{image_name}"
                                        ".jpg".format(image_name=image_name)))
            self.assertFalse(path.exists("{image_name}_0"
                                         ".jpg".format(image_name=image_name)))
            # Test downloading and that it doesn't overwrite 0
            result = runner.invoke(sauce_finder.main, data)
            sleep(3)
            self.assertEqual(result.exit_code, 0)
            self.assertTrue(path.exists("{image_name}"
                                        ".jpg".format(image_name=image_name)))
            self.assertTrue(path.exists("{image_name}_0"
                                        ".jpg".format(image_name=image_name)))
            self.assertFalse(path.exists("{image_name}_1"
                                         ".jpg".format(image_name=image_name)))
            # Test downloading and that it doesn't overwrite 1
            result = runner.invoke(sauce_finder.main, data)
            sleep(3)
            self.assertEqual(result.exit_code, 0)
            self.assertTrue(path.exists("{image_name}"
                                        ".jpg".format(image_name=image_name)))
            self.assertTrue(path.exists("{image_name}_0"
                                        ".jpg".format(image_name=image_name)))
            self.assertTrue(path.exists("{image_name}_1"
                                        ".jpg".format(image_name=image_name)))
            self.assertFalse(path.exists("{image_name}_2"
                                         ".jpg".format(image_name=image_name)))
            # Test downloading and that it doesn't overwrite 2
            result = runner.invoke(sauce_finder.main, data)
            sleep(3)
            self.assertEqual(result.exit_code, 0)
            self.assertTrue(path.exists("{image_name}"
                                        ".jpg".format(image_name=image_name)))
            self.assertTrue(path.exists("{image_name}_0"
                                        ".jpg".format(image_name=image_name)))
            self.assertTrue(path.exists("{image_name}_1"
                                        ".jpg".format(image_name=image_name)))
            self.assertTrue(path.exists("{image_name}_2"
                                        ".jpg".format(image_name=image_name)))
            self.assertFalse(path.exists("{image_name}_3"
                                         ".jpg".format(image_name=image_name)))

    def test_download_with_40_first(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Test downloading only first
            data = ["https://i.imgur.com/ryIgBpv.jpg", "-d"]
            result = runner.invoke(sauce_finder.main, data, input="f\n")
            sleep(3)
            image_name = "__original_drawn_by_kei_jiei__sample-904c3388be198" \
                         "6e691c2a32019a5c3fa"
            self.assertTrue(path.exists("{im}.jpg".format(im=image_name)))
            self.assertFalse(path.exists("{im}_0.jpg".format(im=image_name)))
            self.assertEqual(result.exit_code, 0)
            # Test downloading only first and that it doesn't overwrite
            result = runner.invoke(sauce_finder.main, data, input="f\n")
            sleep(3)
            image_name = "__original_drawn_by_kei_jiei__sample-904c3388be198" \
                         "6e691c2a32019a5c3fa"
            self.assertTrue(path.exists("{im}.jpg".format(im=image_name)))
            self.assertTrue(path.exists("{im}_0.jpg".format(im=image_name)))
            self.assertFalse(path.exists("{im}_1.jpg".format(im=image_name)))
            self.assertEqual(result.exit_code, 0)

    def test_download_with_40_2(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            data = ["https://i.imgur.com/ryIgBpv.jpg", "-d"]
            # Test downloading specific numbers
            result = runner.invoke(sauce_finder.main, data, input="1-2\n")
            sleep(3)
            image_name = "__original_drawn_by_kei_jiei__sample-904c3388be198" \
                         "6e691c2a32019a5c3fa"
            image_name_2 = "__rui_dream_c_club_and_1_more_drawn_by_homare_fo" \
                           "ol_s_art__050ba10cbda097ac3015c5129aa763a4"
            self.assertTrue(path.exists("{im}.jpg".format(im=image_name)))
            self.assertTrue(path.exists("{im}.jpg".format(im=image_name_2)))
            self.assertEqual(result.exit_code, 0)
            # Test downloading specific numbers and that it doesn't overwrite
            result = runner.invoke(sauce_finder.main, data, input="1-2\n")
            sleep(3)
            image_name = "__original_drawn_by_kei_jiei__sample-904c3388be198" \
                         "6e691c2a32019a5c3fa"
            image_name_2 = "__rui_dream_c_club_and_1_more_drawn_by_homare_fo" \
                           "ol_s_art__050ba10cbda097ac3015c5129aa763a4"
            self.assertTrue(path.exists("{im}.jpg".format(im=image_name)))
            self.assertTrue(path.exists("{im}_0.jpg".format(im=image_name)))
            self.assertFalse(path.exists("{im}_1.jpg".format(im=image_name)))
            self.assertTrue(path.exists("{im}.jpg".format(im=image_name_2)))
            self.assertTrue(path.exists("{im}_0.jpg".format(im=image_name_2)))
            self.assertFalse(path.exists("{im}_1.jpg".format(im=image_name_2)))
            self.assertEqual(result.exit_code, 0)

    def test_download_with_40_all(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            data = ["https://i.imgur.com/ryIgBpv.jpg", "-d"]
            # Download all and check that all exists
            result = runner.invoke(sauce_finder.main, data, input="a\n")
            sleep(5)
            self.assertEqual(result.exit_code, 0)
            images = [
                    "__original_drawn_by_kei_jiei__sample-904c3388be1986e691c2"
                    "a32019a5c3fa.jpg",
                    "__rui_dream_c_club_and_1_more_drawn_by_homare_fool_s_art_"
                    "_050ba10cbda097ac3015c5129aa763a4.jpg",
                    "__rui_dream_c_club_and_1_more_drawn_by_homare_fool_s_art_"
                    "_972f011c937de995db7345e6753f2581.jpg",
                    "__rui_dream_c_club_and_1_more_drawn_by_homare_fool_s_art_"
                    "_cd6bef3f5e8f698fd72e3d04133f8b51.jpg",
                    "__shameimaru_aya_touhou_drawn_by_beni_shake__ef99efca9f3a"
                    "0055b4198f1dad2cc941.jpg",
                    "__artoria_pendragon_saber_mordred_and_mordred_fate_and_2_"
                    "more_drawn_by_ouhina__3f8ccde2068ec2c04d8e3aadba1574f3"
                    ".png",
                    "__original_drawn_by_chita_ketchup__sample-5ac138ee675f517"
                    "27a5a6bb43a9ad6f4.jpg",
                    "__serizawa_nao_free_and_1_more_drawn_by_funikurikurara__3"
                    "0007a98c54efb33eed952672b2da24e.jpg",
                    "__hamakaze_kantai_collection_drawn_by_suuzuki_ayato__samp"
                    "le-28bcd82c79f1d35c6acc30a829fc6ccf.jpg",
                    "__minami_kotori_and_kousaka_honoka_love_live_and_1_more_d"
                    "rawn_by_bansaku_kinpikablue__sample-bfee33c9719bb2fe95022"
                    "e5003d19a6c.jpg",
                    "__eminoa_original_and_1_more_drawn_by_akaneko_redakanekoc"
                    "at__sample-2ca2b3148494b1944f5bdce2f2b98235.jpg",
                    "__ushiromiya_battler_and_ushiromiya_ange_umineko_no_naku_"
                    "koro_ni_drawn_by_chajka__1d3faa3471122271a58d2d3dd41b5887"
                    ".jpg",
                    "__minamoto_no_raikou_fate_and_1_more_drawn_by_fuya_tempup"
                    "upu__sample-d1b5536a8a87e08e5070477fe1039815.jpg",
                    "__drawn_by_saibashi__e72e6771e7550b730cdc81e5d80ca391"
                    ".jpg",
                    "__shishigami_bang_blazblue_drawn_by_daimon560__sample-05c"
                    "520fd5e0633fd75d3a1aa7a70611f.jpg"
            ]
            for image in images:
                self.assertTrue(path.exists(f"{image}"))


if __name__ == '__main__':
    unittest.main()
